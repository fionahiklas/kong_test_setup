## Overview

Following instructions for [installing kong under docker](https://docs.konghq.com/install/docker/?_ga=2.107024450.135970580.1551969859-2027916215.1551969859)



## Setup

### Kong With Docker

Creating Kong network

```
docker network create kong-net
```

Starting DB for Kong

```
docker run -d --name kong-database --network=kong-net -p 9042:9042 cassandra:3
```

Boostrap the Kong DB

```
docker run --rm --network=kong-net \
-e "KONG_DATABASE=cassandra" \
-e "KONG_PG_HOST=kong-database" \
-e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
kong:latest kong migrations bootstrap
```

Start Kong

```
docker run -d --name kong \
     --network=kong-net \
     -e "KONG_DATABASE=cassandra" \
     -e "KONG_PG_HOST=kong-database" \
     -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
     -e "KONG_PROXY_ACCESS_LOG=/dev/stdout" \
     -e "KONG_ADMIN_ACCESS_LOG=/dev/stdout" \
     -e "KONG_PROXY_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_ERROR_LOG=/dev/stderr" \
     -e "KONG_ADMIN_LISTEN=0.0.0.0:8001, 0.0.0.0:8444 ssl" \
     -p 8000:8000 \
     -p 8443:8443 \
     -p 8001:8001 \
     -p 8444:8444 \
     kong:latest
```

Test with

```
curl http://localhost:8001/  | json_pp
```

If you shutdown or reboot your machine you can restart the containers using the following commands 

```
docker start kong-database
docker start kong
```


### Test API 

Using [Python Flask](http://flask.pocoo.org/docs/1.0/quickstart/)

Create a virtual environment for python and activate 

```
virtualenv matrix
. ./matrix/bin/activate
```

Install Flask

``` 
pip install flask
```

Run the test application

``` 
python restapp/simple_rest_app.py
```

Test the application

```
curl http://127.0.0.1:5000/hello
curl http://127.0.0.1:5000/status
```

Create the Docker container 
 
```
cd restapp
docker build -t test/simple-rest-app:1.0 .
```

Run the container

``` 
docker run -d \
  --name restapp \
  --network=kong-net \
  -p 5000:5000 \
  test/simple-rest-app:1.0
```

Alternatively, restart an existing container

```
docker start restapp 
```


### Kong Setup for Test API

Creating a service in Kong

```
curl -X POST \
--url "http://127.0.0.1:8001/services" \
--data "name=hello-world-service" \
--data "url=http://restapp:5000" 
```

Add routes 

``` 
curl -X POST \
--url "http://127.0.0.1:8001/services/hello-world-service/routes" \
--data 'hosts[]=hello.eu' \
--data 'paths[]=/hello'  \
--data 'paths[]=/status'
--data 'strip_path=false'
```

Test access to service

``` 
curl --header "Host: hello.eu" http://127.0.0.1:8000/status
curl --header "Host: hello.eu" http://127.0.0.1:8000/hello
```

Setup OAuth2 plugin for service

``` 
curl -X POST http://127.0.0.1:8001/services/hello-world-service/plugins \
    --data "name=oauth2"  \
    --data "config.scopes=email,phone,address" \
    --data "config.mandatory_scope=true" \
    --data "config.enable_client_credentials=true"
```

Create a consumer for OAuth2

``` 
curl -X POST http://127.0.0.1:8001/consumers/ \
    --data "username=terry" \
    --data "custom_id=pratchett"
```

This will produce an output something like this

``` 
{"custom_id":"pratchett","created_at":1553770450,"username":"terry","id":"38eb3a5f-fb7a-4eb8-ac54-1c8e55f1c089"}
```

The `id` from the output above is needed in the next step to 
create the client credentials 

``` 
curl -X POST http://127.0.0.1:8001/consumers/{consumer_id}/oauth2 \
    --data "name=RestApplication" \
    --data "client_id=rincewind" \
    --data "client_secret=potatoes" \
    --data "redirect_uris[]=http://some-domain/endpoint/"
```

### Testing Service Through Kong

Check that you can get a token with the following command

``` 
curl -k -vvv --user rincewind:potatoes -X POST \
    --data "grant_type=client_credentials" \
    --data "scope=email,phone,address" \
    -H "Host: hello.eu" \
    https://127.0.0.1:8443/hello-world-service/oauth2/token
```

This will generate output something like this

```
{"token_type":"bearer","access_token":"kebmkGKJ9mw4luifTQLhS7bHeoXtloAy","expires_in":7200}
```

Call the API again using this token

``` 
curl --header "Host: hello.eu" \
    --header "Authorization: bearer kebmkGKJ9mw4luifTQLhS7bHeoXtloAy" http://127.0.0.1:8000/status
```


### SSL on Endpoints

In order to get SSL certs added to the endpoints there firstly needs to be Certification Authority and Server identity

#### Easy RSA

Using [easy-rsa](https://github.com/OpenVPN/easy-rsa), clone the repo to somewhere, e.g.

``` 
cd $HOME/wd/3rdparty
git clone https://github.com/OpenVPN/easy-rsa.git
```

Adding the script to the path, for example using a command like this

``` 
export PATH=$PATH:$HOME/wd/3rdparty/easy-rsa/easyrsa3
```

Create directory structure

```
mkdir -p crypto/ca
mkdir -p crypto/server 
```


#### Creating Certification Authority 

Run the following commands

``` 
cd crypto/ca
easyrsa init-pki
easyrsa build-ca
```

Use a passphrase like `password` or something similarly short/memorable - this isn't a production system, 
it's just for testing purposes.  Also set the CA name to `kong-ca`


#### Creating Server Certificate

Create the signing request

``` 
cd crypto/server
easyrsa init-pki
easyrsa --subject-alt-name=DNS:hello.eu gen-req kong-server
```

You can check that the subject alternative name has been applied to the request with this command

Signing request

``` 
cd ../ca
easyrsa import-req ../server/pki/reqs/kong-server.req kong-server
easyrsa sign-req server kong-server
```

#### Creating SSL Setup in Kong

Firstly remove the passphrase on the server key 

``` 
cd crypto/server/pki/private
mv kong-server.key kong-server-enc.key
openssl rsa -in kong-server-enc.key -out kong-server.key
```

Create a certificate 

```
curl -X POST http://127.0.0.1:8001/certificates \
-F "cert=@crypto/ca/pki/issued/kong-server.crt" \
-F "key=@crypto/server/pki/private/kong-server.key" \
-F "snis=hello.eu"
```

## References

### Kong

* [Kong docs](https://docs.konghq.com/)
* [Kong hello world](https://github.com/Kong/kong-oauth2-hello-world)
* [Kong API](https://docs.konghq.com/0.13.x/admin-api/)
* [Kong OAuth Plugin](https://docs.konghq.com/hub/kong-inc/oauth2/)


### Flask

* [Skeleton Flask app](https://dev.to/aaahmedaa/create-restapi-for-your-python-scripts-using-flask-with-15-line-of-code-10ml)
* [Flask quick start](http://flask.pocoo.org/docs/1.0/quickstart/)