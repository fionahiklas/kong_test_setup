from flask import Flask
app = Flask(__name__)


@app.route('/hello')
def hello_world():
    return 'Hello, World!'


@app.route('/status')
def status():
    return '{ "status":  "ok" }'


if __name__ == '__main__':
    app.run(host='0.0.0.0')




